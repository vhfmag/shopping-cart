# App de carrinho de compras

Neste repositório se encontram o código-fonte do servidor e do cliente de um app de loja.

## Acessando o app

A loja virtual pode ser encontrada em https://challenge-web-cart.now.sh/, e um playground GraphQL da API pode ser encontrado em https://challenge-api-cart.now.sh/graphql. Os links são atualizados a cada deploy através de CI/CD, e este é o método recomendado para consumir o app.

## Rodando o app localmente

Para rodar a aplicação localmente, será necessário:

- Ter uma instância do MongoDB, local ou remota, disponível para que a API a utilize. Para tal, basta passar a URL da instância pra variável de ambiente MONGO_URL
- Para inicializar o catálogo de produtos, faz-se uso da API do [The Movie DB](https://themoviedb.org/), cuja chave deve ser disponibilizada na variável de ambiente TMDB_API_KEY para que a `mutation` `scaffold` funcione e initialize a base de dados.

Tendo isso feito:

- Rode `yarn dev` em `src/client`. Isso vai disponibilizar o cliente na URL http://localhost:3000
- Rode `yarn dev` em `src/server`. Isso vai disponibilizar a API na URL http://localhost:3002 (um playground da API GraphQL será disponibilizado em http://localhost:3002/graphql)

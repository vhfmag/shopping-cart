declare module "immer-wieder" {
	import * as React from "react";

	export default function createContext<State>(
		fn: (
			setState: (
				fnOrState: Partial<State> | ((st: State) => Partial<State>),
				cb?: (st: State) => void,
			) => void,
			getState: (st: State) => void,
		) => State,
	): {
		Provider: React.ComponentType;
		Consumer: React.ComponentType<{
			children: React.SFC<State>;
		}>;
	};
}

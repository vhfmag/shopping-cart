import { graphql } from "react-relay";
import { Environment, commitRelayModernMutation } from "relay-runtime";
import {
	addToCartMutationVariables,
	addToCartMutation,
} from "../relay/__generated__/addToCartMutation.graphql";

const commitMutation: typeof commitRelayModernMutation = require("relay-runtime")
	.commitMutation;

const addToCartMutationQuery = graphql`
	mutation addToCartMutation($cartId: ID!, $movieId: ID!) {
		addToCart(cartId: $cartId, movieId: $movieId) {
			id
			items {
				id
				count
				...CartItem_cartItem

				item {
					...MovieCard_movie
				}
			}
		}
	}
`;

export function addToCart(
	environment: Environment,
	variables: addToCartMutationVariables,
) {
	return commitMutation<addToCartMutation>(environment, {
		mutation: addToCartMutationQuery,
		configs: [],
		variables,
		onCompleted: console.log,
		onError: console.error,
	});
}

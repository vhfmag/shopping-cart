import { graphql } from "react-relay";
import { Environment, commitRelayModernMutation } from "relay-runtime";
import {
	changeCartCountMutationVariables,
	changeCartCountMutation,
} from "../relay/__generated__/changeCartCountMutation.graphql";

const commitMutation: typeof commitRelayModernMutation = require("relay-runtime")
	.commitMutation;

const changeCartCountMutationQuery = graphql`
	mutation changeCartCountMutation($cartId: ID!, $movieId: ID!, $count: Int!) {
		changeCartCount(cartId: $cartId, movieId: $movieId, count: $count) {
			id
			items {
				id
				count
				...CartItem_cartItem

				item {
					...MovieCard_movie
				}
			}
		}
	}
`;

export function changeCartCount(
	environment: Environment,
	variables: changeCartCountMutationVariables,
) {
	return commitMutation<changeCartCountMutation>(environment, {
		mutation: changeCartCountMutationQuery,
		configs: [],
		variables,
		onCompleted: console.log,
		onError: console.error,
	});
}

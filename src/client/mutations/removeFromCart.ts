import { graphql } from "react-relay";
import { Environment, commitRelayModernMutation } from "relay-runtime";
import {
	removeFromCartMutationVariables,
	removeFromCartMutation,
} from "../relay/__generated__/removeFromCartMutation.graphql";

const commitMutation: typeof commitRelayModernMutation = require("relay-runtime")
	.commitMutation;

const removeFromCartMutationQuery = graphql`
	mutation removeFromCartMutation($cartId: ID!, $movieId: ID!) {
		removeFromCart(cartId: $cartId, movieId: $movieId) {
			id
			items {
				id
				...CartItem_cartItem

				item {
					id
					...MovieCard_movie
				}
			}
		}
	}
`;

export function removeFromCart(
	environment: Environment,
	variables: removeFromCartMutationVariables,
) {
	return commitMutation<removeFromCartMutation>(environment, {
		mutation: removeFromCartMutationQuery,
		configs: [],
		variables,
		onCompleted: console.log,
		onError: console.error,
	});
}

const withTypescript = require("@zeit/next-typescript");
const webpack = require("webpack");

module.exports = withTypescript({
	webpack(config) {
		config.plugins.push(
			new webpack.EnvironmentPlugin({
				APP_WEB_URL: undefined,
				APP_API_URL: "localhost:3002",
			}),
		);

		return config;
	},
});

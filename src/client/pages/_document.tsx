import * as React from "react";
import * as PropTypes from "prop-types";
import Document, {
	Head,
	Main,
	NextScript,
	NextDocumentContext,
} from "next/document";
const flush = require("styled-jsx/server");

class MyDocument extends Document {
	public getInitialProps = (ctx: NextDocumentContext) => {
		let pageContext: any;
		const page = ctx.renderPage(Component => {
			const WrappedComponent: React.SFC<any> = props => {
				pageContext = props.pageContext;
				return <Component {...props} />;
			};

			WrappedComponent.propTypes = {
				pageContext: PropTypes.object.isRequired,
			};

			return WrappedComponent;
		});

		return {
			...page,
			pageContext,
			styles: (
				<React.Fragment>
					<style
						id="jss-server-side"
						dangerouslySetInnerHTML={{
							__html: pageContext.sheetsRegistry.toString(),
						}}
					/>
					{flush() || null}
				</React.Fragment>
			),
		};
	};

	public render() {
		const { pageContext } = this.props;

		return (
			<html lang="en" dir="ltr">
				<Head>
					<title>The Last Movie Shop on Earth</title>
					<meta charSet="utf-8" />
					<meta
						name="viewport"
						content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
					/>
					<meta
						name="theme-color"
						content={pageContext && pageContext.theme.palette.primary.main}
					/>
					<link
						rel="stylesheet"
						href="https://fonts.googleapis.com/css?family=Roboto:300,400,500"
					/>
				</Head>
				<body>
					<Main />
					<NextScript />
				</body>
			</html>
		);
	}
}

export default MyDocument;

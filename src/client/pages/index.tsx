import * as React from "react";
import { graphql, QueryRenderer, ReadyState } from "react-relay";
import { environment } from "../relay/environment";
import {
	pagesQueryResponse,
	pagesQuery,
} from "../relay/__generated__/pagesQuery.graphql";
import {
	withStyles,
	StyledComponentProps,
	CircularProgress,
	Typography,
} from "@material-ui/core";
import { Error } from "@material-ui/icons";
import { MovieCard } from "../src/molecules/MovieCard";
import { Provider, Consumer, IGlobalState } from "../src/store";
import { ApiError, EXPIRED_CART, NOT_FOUND } from "../errorTypes";
import { Header } from "../src/molecules/Header";
import { addToCart } from "../mutations/addToCart";

interface IInnerIndexPageProps
	extends ReadyState<pagesQueryResponse>,
		StyledComponentProps {
	cartId: IGlobalState["cartId"];
	setCartId: IGlobalState["actions"]["setCartId"];
}

const IndexPageComponent = withStyles({
	"@global": {
		"*": {
			boxSizing: "border-box",
		},
		"body, html, #__next": {
			margin: 0,
			minHeight: "100vh",
		},
		"#__next": {
			display: "flex",
			flexDirection: "column",
		},
	},
	nonIdealOverlay: {
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		flexGrow: 1,
	},
	flexGrow: {
		flexGrow: 1,
	},
	productGrid: {
		flexGrow: 1,
		display: "grid",
		gridGap: "20px",
		padding: "20px",
		gridTemplateColumns: "repeat(auto-fit, minmax(250px, 1fr))",
		alignItems: "flex-start",
	},
})(
	class InnerIndexPage extends React.Component<IInnerIndexPageProps> {
		private syncCartWithLocalStorage() {
			const { props, error, cartId, setCartId } = this.props;

			if (error) {
				if (ApiError.isApiError(error)) {
					if (error.type === EXPIRED_CART || error.type === NOT_FOUND) {
						setCartId(undefined);
					}
				}
			} else if (props && props.cart.id !== cartId) {
				setCartId(props.cart.id);
			}
		}

		public componentDidMount() {
			this.syncCartWithLocalStorage();
		}

		public componentDidUpdate() {
			this.syncCartWithLocalStorage();
		}

		private addToCart = (movieId: string) => {
			if (!this.props.props) {
				return;
			}

			addToCart(environment, {
				movieId,
				cartId: this.props.props.cart.id,
			});
		};

		public render() {
			const { classes, error, props } = this.props;

			return (
				<>
					<Header cart={(props && props.cart) || null} />
					{error ? (
						<div className={classes!.nonIdealOverlay}>
							<Error />
							<Typography variant="subheading" component="div">
								Ocorreu um erro
							</Typography>
						</div>
					) : !props ? (
						<div className={classes!.nonIdealOverlay}>
							<CircularProgress />
						</div>
					) : (
						<div className={classes!.productGrid}>
							{props.feed.map(movie => (
								<MovieCard
									key={movie.id}
									addToCart={this.addToCart}
									movie={movie}
								/>
							))}
						</div>
					)}
				</>
			);
		}
	},
);

export default function IndexPage() {
	return (
		<Provider>
			<Consumer>
				{({ cartId, actions: { setCartId } }) => (
					<QueryRenderer<pagesQuery>
						environment={environment}
						render={props => (
							<IndexPageComponent
								cartId={cartId}
								setCartId={setCartId}
								{...props}
							/>
						)}
						variables={{ cartId }}
						query={indexPageQuery}
					/>
				)}
			</Consumer>
		</Provider>
	);
}

export const indexPageQuery = graphql`
	query pagesQuery($cartId: ID) {
		feed {
			id
			...MovieCard_movie
		}
		cart(cartId: $cartId) {
			id
			...Header_cart
		}
	}
`;

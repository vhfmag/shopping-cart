import {
	Environment,
	Network,
	RecordSource,
	Store,
	FetchFunction,
} from "relay-runtime";

const fetchQuery: FetchFunction = (operation, variables) => {
	return fetch(
		`${process.env.APP_API_PROTOCOL || "https"}://${
			process.env.APP_API_URL
		}/graphql`,
		{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				query: operation.text,
				variables,
			}),
		},
	).then(response => {
		return response.json();
	});
};

export const environment = new Environment({
	network: Network.create(fetchQuery),
	store: new Store(new RecordSource()),
});

import * as React from "react";
import {
	StyledComponentProps,
	withStyles,
	ListItem,
	ListItemText,
	ListItemSecondaryAction,
	IconButton,
} from "@material-ui/core";
import { createFragmentContainer, graphql, RelayProp } from "react-relay";
import { Delete, Add, Remove } from "@material-ui/icons";
import { CartItem_cartItem } from "../../relay/__generated__/CartItem_cartItem.graphql";
import { currencyFormatter } from "../utils/formatters";

export interface ICartItemProps extends StyledComponentProps {
	relay: RelayProp;
	cartItem: CartItem_cartItem;
	removeFromCart(movieId: string): void;
	changeCount(movieId: string, count: number): void;
}

class InnerCartItem extends React.Component<ICartItemProps> {
	private removeFromCart = () =>
		this.props.removeFromCart(this.props.cartItem.item.id);

	private incrementItem = () =>
		this.props.changeCount(this.props.cartItem.item.id, 1);

	private decrementItem = () =>
		this.props.changeCount(this.props.cartItem.item.id, -1);

	public render() {
		const { cartItem } = this.props;
		const floatPrice = cartItem.item.price / 100;

		return (
			<ListItem>
				<ListItemText
					primary={cartItem.item.title}
					secondary={`${cartItem.count}x ${currencyFormatter(
						floatPrice,
					)} — ${currencyFormatter(cartItem.count * floatPrice)}`}
				/>
				<ListItemSecondaryAction>
					<IconButton
						disabled={cartItem.item.stock === 0}
						onClick={this.incrementItem}
					>
						<Add />
					</IconButton>
					<IconButton
						disabled={cartItem.count <= 1}
						onClick={this.decrementItem}
					>
						<Remove />
					</IconButton>
					<IconButton onClick={this.removeFromCart}>
						<Delete />
					</IconButton>
				</ListItemSecondaryAction>
			</ListItem>
		);
	}
}

export const CartItem = createFragmentContainer(
	withStyles({})(InnerCartItem),
	graphql`
		fragment CartItem_cartItem on CartItem {
			count
			item {
				id
				title
				price
				stock
			}
		}
	`,
);

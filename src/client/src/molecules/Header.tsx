import * as React from "react";
import {
	Typography,
	withStyles,
	StyledComponentProps,
	AppBar,
	Toolbar,
	IconButton,
	Badge,
	Drawer,
	Divider,
	List,
	ListItem,
	ListSubheader,
	ListItemText,
} from "@material-ui/core";
import { ShoppingCart, Close } from "@material-ui/icons";
import { createFragmentContainer, graphql, RelayProp } from "react-relay";
import { Header_cart } from "../../relay/__generated__/Header_cart.graphql";
import { CartItem } from "./CartItem";
import { removeFromCart } from "../../mutations/removeFromCart";
import { changeCartCount } from "../../mutations/changeCartCount";
import { currencyFormatter } from "../utils/formatters";

interface IHeaderState {
	isDrawerOpen?: boolean;
}

interface IHeaderProps extends StyledComponentProps {
	cart: Header_cart | null;
	relay: RelayProp;
}

class InnerHeader extends React.Component<IHeaderProps, IHeaderState> {
	public state: IHeaderState = { isDrawerOpen: false };

	private openDrawer = () => this.setState({ isDrawerOpen: true });
	private closeDrawer = () => this.setState({ isDrawerOpen: false });

	private removeFromCart = (movieId: string) => {
		const { cart } = this.props;

		if (!cart) {
			return;
		}

		removeFromCart(this.props.relay.environment, { movieId, cartId: cart.id });
	};

	private changeCartCount = (movieId: string, delta: number) => {
		const { cart } = this.props;

		if (!cart) {
			return;
		}

		changeCartCount(this.props.relay.environment, {
			movieId,
			cartId: cart.id,
			count: delta,
		});
	};

	public render() {
		const { classes, cart } = this.props;
		const productCount =
			cart && cart.items.reduce((acc, item) => acc + item.count, 0);
		const totalPrice =
			(cart &&
				cart.items.reduce(
					(acc, item) => acc + item.count * item.item.price,
					0,
				) / 100) ||
			0;

		return (
			<AppBar position="sticky">
				<Toolbar>
					<Typography variant="headline" component="h1">
						The Last Movie Shop on Earth™
					</Typography>

					<div className={classes!.flexGrow} />

					<Typography variant="subheading">
						{currencyFormatter(totalPrice)}
					</Typography>
					<IconButton onClick={this.openDrawer}>
						{productCount ? (
							<Badge color="secondary" badgeContent={productCount}>
								<ShoppingCart />
							</Badge>
						) : (
							<ShoppingCart />
						)}
					</IconButton>
				</Toolbar>
				<Drawer
					anchor="right"
					onClose={this.closeDrawer}
					classes={{ paper: classes!.drawer }}
					open={this.state.isDrawerOpen}
				>
					<Toolbar>
						<Typography variant="title">Carrinho</Typography>
						<div className={classes!.flexGrow} />
						<IconButton onClick={this.closeDrawer}>
							<Close />
						</IconButton>
					</Toolbar>
					<Divider />
					<List>
						<ListSubheader>Produtos</ListSubheader>
						{cart &&
							cart.items.map(item => (
								<CartItem
									key={item.id}
									cartItem={item}
									changeCount={this.changeCartCount}
									removeFromCart={this.removeFromCart}
								/>
							))}
						<ListSubheader>Total</ListSubheader>
						<ListItem>
							<ListItemText primary={currencyFormatter(totalPrice)} />
						</ListItem>
					</List>
				</Drawer>
			</AppBar>
		);
	}
}

export const Header = createFragmentContainer(
	withStyles({
		flexGrow: {
			flexGrow: 1,
		},
		drawer: {
			width: "500px",
			maxWidth: "100vw",
		},
	})(InnerHeader),
	graphql`
		fragment Header_cart on Cart {
			id
			items {
				id
				count

				item {
					price
				}

				...CartItem_cartItem
			}
		}
	`,
);

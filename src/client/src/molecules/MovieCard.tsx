import * as React from "react";
import {
	Card,
	CardMedia,
	CardContent,
	Typography,
	withStyles,
	StyledComponentProps,
	CardActions,
	Button,
	Collapse,
	IconButton,
	Chip,
} from "@material-ui/core";
import classnames from "classnames";
import { ExpandMore } from "@material-ui/icons";
import { createFragmentContainer, graphql, RelayProp } from "react-relay";
import { MovieCard_movie } from "../../relay/__generated__/MovieCard_movie.graphql";
import { currencyFormatter } from "../utils/formatters";

interface IMovieCardState {
	isExpanded?: boolean;
}

interface IMovieCardProps extends StyledComponentProps {
	movie: MovieCard_movie;
	relay: RelayProp;
	addToCart(movieId: string): void;
}

class InnerMovieCard extends React.Component<IMovieCardProps, IMovieCardState> {
	public state: IMovieCardState = { isExpanded: false };

	private addToCart = () => this.props.addToCart(this.props.movie.id);
	private toggleOpen = () =>
		this.setState(state => ({ isExpanded: !state.isExpanded }));

	public render() {
		const {
			classes,
			movie: { posterUrl, title, price, overview, stock },
		} = this.props;

		return (
			<Card>
				<CardMedia className={classes!.poster} image={posterUrl || undefined} />
				<CardContent>
					<Typography gutterBottom variant="headline" component="h2">
						{title}
					</Typography>
					<div className={classes!.chipGrid}>
						<Chip label={currencyFormatter(price / 100)} color="primary" />
						<Chip label={`Estoque: ${stock}`} color="primary" />
					</div>
				</CardContent>
				<CardActions>
					<Button
						disabled={stock === 0}
						onClick={this.addToCart}
						title={stock === 0 ? "Out of stock" : undefined}
					>
						Add to cart
					</Button>
					<IconButton
						className={classnames(
							classes!.expand,
							this.state.isExpanded && classes!.expanded,
						)}
						onClick={this.toggleOpen}
					>
						<ExpandMore />
					</IconButton>
				</CardActions>
				<Collapse in={this.state.isExpanded} timeout="auto" unmountOnExit>
					<CardContent>
						<Typography paragraph>{overview}</Typography>
					</CardContent>
				</Collapse>
			</Card>
		);
	}
}

export const MovieCard = createFragmentContainer(
	withStyles(theme => ({
		poster: {
			height: 300,
		},
		chipGrid: {
			display: "inline-grid",
			gridGap: "4pt",
			gridAutoFlow: "column",
		},
		expand: {
			transform: "rotate(0deg)",
			transition: theme.transitions.create("transform", {
				duration: theme.transitions.duration.shortest,
			}),
			marginLeft: "auto",
		},
		expanded: {
			transform: "rotate(180deg)",
		},
	}))(InnerMovieCard),
	graphql`
		fragment MovieCard_movie on Movie {
			id
			title
			posterUrl
			stock
			price
			overview
		}
	`,
);

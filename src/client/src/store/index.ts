import createContext from "immer-wieder";

const cartIdCacheKey = "CART_ID";

export interface IGlobalState {
	cartId: string | undefined;
	actions: {
		setCartId: (cartId: string | undefined) => void;
	};
}

export const { Consumer, Provider } = createContext<IGlobalState>(setState => ({
	actions: {
		setCartId: cartId =>
			setState({ cartId }, () => {
				if (typeof localStorage === "undefined") {
					return;
				}

				if (cartId !== undefined) {
					localStorage.setItem(cartIdCacheKey, cartId);
				} else {
					localStorage.removeItem(cartIdCacheKey);
				}
			}),
	},
	cartId:
		(typeof localStorage !== "undefined" &&
			localStorage.getItem(cartIdCacheKey)) ||
		undefined,
}));

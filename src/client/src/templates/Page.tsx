import * as React from "react";

export class PageComponent extends React.Component {
	render() {
		return (
			<div>
				<header>Cabeçalho</header>
				<main>{this.props.children}</main>
			</div>
		);
	}
}

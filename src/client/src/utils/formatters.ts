export const currencyFormatter = Intl.NumberFormat("pt-BR", {
	currency: "BRL",
	maximumFractionDigits: 2,
	minimumFractionDigits: 2,
	minimumIntegerDigits: 1,
	useGrouping: true,
	style: "currency",
}).format;

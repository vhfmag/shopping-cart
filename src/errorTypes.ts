export class ApiError<T extends string> extends Error {
	public constructor(message: string, public type: T) {
		super(message);
	}

	public static isApiError(error: Error): error is ApiError<string> {
		return "type" in error && typeof (error as ApiError<any>).type === "string";
	}
}

export const NOT_FOUND = "NOT_FOUND";
export class NotFoundError extends ApiError<typeof NOT_FOUND> {
	constructor({ entityName, id }: { entityName: string; id: any }) {
		super(
			`No ${entityName} could be found with given id (${id.toString()})`,
			NOT_FOUND,
		);
	}
}

export const INSUFFICIENT_STOCK = "INSUFFICIENT_STOCK";
export class InsufficientStockError extends ApiError<
	typeof INSUFFICIENT_STOCK
> {
	constructor() {
		super("Insufficient stock", INSUFFICIENT_STOCK);
	}
}

export const INSUFFICIENT_ITEM_COUNT = "INSUFFICIENT_ITEM_COUNT";
export class InsufficientItemCountError extends ApiError<
	typeof INSUFFICIENT_ITEM_COUNT
> {
	constructor() {
		super("Insufficient cart item count", INSUFFICIENT_ITEM_COUNT);
	}
}

export const EXPIRED_CART = "EXPIRED_CART";
export class ExpiredCartError extends ApiError<typeof EXPIRED_CART> {
	constructor() {
		super("Cart has expired", EXPIRED_CART);
	}
}

export const MISSING_ITEM = "MISSING_ITEM";
export class MissingItemError extends ApiError<typeof MISSING_ITEM> {
	constructor({ itemId, cartId }: { itemId: any; cartId: any }) {
		super(
			`Item '${itemId}' couldn't be found in given cart '${cartId}'`,
			MISSING_ITEM,
		);
	}
}

export const AssertionFailed = "AssertionFailed";
export class AssertionFailedError extends ApiError<typeof AssertionFailed> {
	constructor({ message }: { message: string }) {
		super(`Assertion failed: ${message}`, AssertionFailed);
	}
}

import { DBSetup } from "./db";
import { MongoClient, ObjectID } from "mongodb";
import { MONGO_URL, DATABASE_NAME } from "./utils/consts";
import { Movie, CartItem, Cart } from "./typings/types";
import { Omit } from "./types";
import { NotFoundError } from "./errorTypes";

export interface IMovieEntry extends Omit<Movie, "id"> {
	_id: ObjectID;
	tmdb_id: string;
}

export function entryToMovie({ _id, tmdb_id, ...rest }: IMovieEntry): Movie {
	return { ...rest, id: _id.toHexString() };
}

export interface ICartEntry {
	_id: ObjectID;
	items: ICartItemEntry[];
	lastUpdated: Date;
}

export interface ICartItemEntry extends Omit<CartItem, "item" | "id"> {
	_id: ObjectID;
	itemId: ObjectID;
}

export const entryToCartItem = ({
	collections: { Movies },
}: DBSetup) => async ({
	itemId,
	_id,
	...rest
}: ICartItemEntry): Promise<CartItem> => {
	const itemEntry = await Movies.findOne({ _id: new ObjectID(itemId) });
	const item = itemEntry && entryToMovie(itemEntry);

	if (!item) {
		throw new NotFoundError({ entityName: "movie", id: itemId });
	}

	return { ...rest, item, id: _id.toHexString() };
};

export const cartItemToEntry = ({
	collections: { Movies },
}: DBSetup) => async ({
	item,
	id,
	...rest
}: CartItem): Promise<ICartItemEntry> => {
	if (!(await Movies.findOne({ _id: new ObjectID(item.id) }))) {
		throw new NotFoundError({ entityName: "movie", id: item.id });
	}

	return { ...rest, itemId: new ObjectID(item.id), _id: new ObjectID(id) };
};

export const entryToCart = (setup: DBSetup) => async ({
	items,
	_id,
}: ICartEntry): Promise<Cart> => ({
	id: _id.toHexString(),
	items: await Promise.all(items.map(entryToCartItem(setup))),
});

export async function connectToDatabase() {
	const client = await MongoClient.connect(MONGO_URL!);
	const db = client.db(DATABASE_NAME);

	const Movies = db.collection<IMovieEntry>("movies");
	const Carts = db.collection<ICartEntry>("carts");

	return { client, db, collections: { Movies, Carts } };
}

type PromisedType<P extends PromiseLike<any>> = P extends PromiseLike<infer Ret>
	? Ret
	: never;

export type DBSetup = PromisedType<ReturnType<typeof connectToDatabase>>;

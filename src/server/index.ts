import { start } from "./server";
import { connectToDatabase } from "./db";

async function main() {
	const setup = await connectToDatabase();
	await start(setup);
}

main();

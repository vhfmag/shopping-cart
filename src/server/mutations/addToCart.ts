import { MutationResolvers } from "../typings/types";
import { DBSetup, entryToCart } from "../db";
import { throwIfNullish } from "../utils/helpers";
import {
	NotFoundError,
	InsufficientStockError,
	AssertionFailedError,
} from "../errorTypes";
import { getCartWithId, getRemainingCountOfItem } from "../shared";
import { ObjectID } from "bson";

export const addToCartMutation = (
	setup: DBSetup,
): MutationResolvers.Resolvers["addToCart"] => async (
	_,
	{ movieId, cartId },
) => {
	const {
		collections: { Movies, Carts },
	} = setup;

	const movie = throwIfNullish(
		await Movies.findOne({ _id: new ObjectID(movieId) }),
		new NotFoundError({ entityName: "movie", id: movieId }),
	);

	const cartEntry = throwIfNullish(
		await getCartWithId(cartId, setup),
		new NotFoundError({ entityName: "cart", id: cartId }),
	);

	const cartItem = cartEntry.items.find(item => item.itemId.equals(movieId));

	const remainingAmount = await getRemainingCountOfItem(movie._id, setup);

	if (remainingAmount < 1) {
		throw new InsufficientStockError();
	}

	const newItems = cartItem
		? cartEntry.items.map(
				item =>
					item.itemId.equals(movieId)
						? {
								...item,
								count: item.count + 1,
						  }
						: item,
		  )
		: cartEntry.items.concat({
				_id: new ObjectID(),
				count: 1,
				itemId: movie._id,
		  });

	await Carts.updateOne(
		{ _id: new ObjectID(cartId) },
		{
			$set: { items: newItems, lastUpdated: new Date() },
		},
	);

	await Movies.updateOne(
		{ _id: new ObjectID(movieId) },
		{ $inc: { stock: -1 } },
	);

	return entryToCart(setup)(
		throwIfNullish(
			await getCartWithId(cartId, setup),
			new AssertionFailedError({
				message: "cart wasn't found after creation",
			}),
		),
	);
};

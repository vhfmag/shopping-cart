import { MutationResolvers } from "../typings/types";
import { DBSetup, entryToCart } from "../db";
import { throwIfNullish } from "../utils/helpers";
import {
	NotFoundError,
	MissingItemError,
	InsufficientItemCountError,
	InsufficientStockError,
	AssertionFailedError,
} from "../errorTypes";
import { getCartWithId, getRemainingCountOfItem } from "../shared";
import { ObjectID } from "bson";

export const changeCartCountMutation = (
	setup: DBSetup,
): MutationResolvers.Resolvers["changeCartCount"] => async (
	_,
	{ movieId, cartId, count },
) => {
	const {
		collections: { Carts, Movies },
	} = setup;

	throwIfNullish(
		await Movies.findOne({ _id: new ObjectID(movieId) }),
		new NotFoundError({ entityName: "movie", id: movieId }),
	);

	const cart = throwIfNullish(
		await getCartWithId(cartId, setup),
		new NotFoundError({ entityName: "cart", id: cartId }),
	);

	const cartItem = cart.items.find(item => item.itemId.equals(movieId));

	if (!cartItem) {
		throw new MissingItemError({ itemId: movieId, cartId });
	} else if (cartItem.count + count <= 0) {
		throw new InsufficientItemCountError();
	} else if (
		count > 0 &&
		(await getRemainingCountOfItem(new ObjectID(movieId), setup)) < count
	) {
		throw new InsufficientStockError();
	}

	const newItems = cart.items.map(
		item => (item === cartItem ? { ...item, count: item.count + count } : item),
	);

	await Carts.updateOne(
		{ _id: new ObjectID(cartId) },
		{
			$set: { items: newItems, lastUpdated: new Date() },
		},
	);

	await Movies.updateOne(
		{ _id: new ObjectID(movieId) },
		{ $inc: { stock: -count } },
	);

	return entryToCart(setup)(
		throwIfNullish(
			await getCartWithId(cartId, setup),
			new AssertionFailedError({
				message: "cart was deleted during function execution",
			}),
		),
	);
};

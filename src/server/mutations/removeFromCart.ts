import { DBSetup, entryToCart } from "../db";
import { MutationResolvers } from "../typings/types";
import { throwIfNullish } from "../utils/helpers";
import {
	NotFoundError,
	MissingItemError,
	AssertionFailedError,
} from "../errorTypes";
import { getCartWithId } from "../shared";
import { ObjectID } from "bson";

export const removeFromCartMutation = (
	setup: DBSetup,
): MutationResolvers.Resolvers["removeFromCart"] => async (
	_,
	{ movieId, cartId },
) => {
	const {
		collections: { Movies, Carts },
	} = setup;

	throwIfNullish(
		await Movies.findOne({ _id: new ObjectID(movieId) }),
		new NotFoundError({ entityName: "movie", id: movieId }),
	);

	const cart = throwIfNullish(
		await getCartWithId(cartId, setup),
		new NotFoundError({ entityName: "cart", id: cartId }),
	);

	const newItems = cart.items.filter(item => !item.itemId.equals(movieId));
	const deletedItem = cart.items.find(item => item.itemId.equals(movieId));

	if (!deletedItem) {
		throw new MissingItemError({ cartId, itemId: movieId });
	}

	await Carts.updateOne(
		{ _id: new ObjectID(cartId) },
		{
			$set: { items: newItems, lastUpdated: new Date() },
		},
	);

	await Movies.updateOne(
		{ _id: new ObjectID(movieId) },
		{ $inc: { stock: deletedItem.count } },
	);

	return entryToCart(setup)(
		throwIfNullish(
			await getCartWithId(cartId, setup),
			new AssertionFailedError({
				message: "cart was deleted during function execution",
			}),
		),
	);
};

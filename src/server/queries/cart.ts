import { DBSetup, ICartEntry, entryToCart } from "../db";
import { QueryResolvers } from "../typings/types";
import { getCartWithId } from "../shared";
import { throwIfNullish } from "../utils/helpers";
import { ApiError, EXPIRED_CART, AssertionFailedError } from "../errorTypes";

async function createCart(setup: DBSetup) {
	const {
		collections: { Carts },
	} = setup;

	const cartId = (await Carts.insertOne({
		items: [],
		lastUpdated: new Date(),
	})).insertedId;

	const cartEntry = await getCartWithId(cartId, setup);

	return throwIfNullish(
		cartEntry,
		new AssertionFailedError({
			message: "cart is nullish right after new cart creation",
		}),
	);
}

export const cartQuery = (
	setup: DBSetup,
): QueryResolvers.Resolvers["cart"] => async (_, { cartId }) => {
	const {
		collections: { Carts },
	} = setup;

	let cart: ICartEntry;

	if (cartId) {
		try {
			const cartEntry = await getCartWithId(cartId, setup);
			if (cartEntry) {
				cart = cartEntry;
			} else {
				cart = await createCart(setup);
			}
		} catch (error) {
			console.error(error);
			if (ApiError.isApiError(error) && error.type === EXPIRED_CART) {
				cart = await createCart(setup);
			} else {
				throw error;
			}
		}
	} else {
		cart = await createCart(setup);
	}

	Carts.updateOne(
		{ _id: cart._id },
		{
			$set: { lastUpdated: new Date() },
		},
	);

	return await entryToCart(setup)(cart);
};

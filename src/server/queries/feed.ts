import { QueryResolvers } from "../typings/types";
import { DBSetup, entryToMovie } from "../db";

export const feedQuery = ({
	collections: { Movies },
}: DBSetup): QueryResolvers.Resolvers["feed"] => (_, { count }) => {
	return Movies.aggregate([
		{ $match: { stock: { $gt: 0 } } },
		{ $sort: { voteAverage: 1, voteCount: 1 } },
		{ $limit: count || 10 },
	])
		.toArray()
		.then(movie => movie.map(entryToMovie));
};

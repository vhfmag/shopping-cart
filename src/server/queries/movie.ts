import { DBSetup, entryToMovie } from "../db";
import { throwIfNullish } from "../utils/helpers";
import { ObjectID } from "bson";
import { NotFoundError } from "../errorTypes";
import { QueryResolvers } from "../typings/types";

export const movieQuery = ({
	collections: { Movies },
}: DBSetup): QueryResolvers.Resolvers["movie"] => async (_, { movieId }) => {
	return entryToMovie(
		throwIfNullish(
			await Movies.findOne({ _id: new ObjectID(movieId) }),
			new NotFoundError({ entityName: "movie", id: movieId }),
		),
	);
};

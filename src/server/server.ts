import { readFileSync } from "fs";
import { join } from "path";
import {
	MutationResolvers,
	QueryResolvers,
	CartItemResolvers,
	GenreResolvers,
	MovieResolvers,
} from "./typings/types";

import express from "express";
import { APP_API_URL, APP_API_PORT, APP_API_PROTOCOL } from "./utils/consts";
import { DBSetup } from "./db";

import { ApolloServer } from "apollo-server-express";
import { makeExecutableSchema, IResolvers } from "graphql-tools";
import { feedQuery } from "./queries/feed";
import { cartQuery } from "./queries/cart";
import { movieQuery } from "./queries/movie";
import { addToCartMutation } from "./mutations/addToCart";
import { changeCartCountMutation } from "./mutations/changeCartCount";
import { removeFromCartMutation } from "./mutations/removeFromCart";
import { scaffoldDatabase } from "./setup";

const app = express();

const gqlSchema = readFileSync(join(__dirname, "./schema.graphql"), {
	encoding: "utf8",
});

interface ISchemaResolvers {
	Query: QueryResolvers.Resolvers;
	Mutation: MutationResolvers.Resolvers;
	CartItem: CartItemResolvers.Resolvers;
	Genre: GenreResolvers.Resolvers;
	Movie: MovieResolvers.Resolvers;
}

export const start = async (setup: DBSetup) => {
	try {
		const typeDefs = [gqlSchema];

		const resolvers: ISchemaResolvers = {
			Query: {
				feed: feedQuery(setup),
				cart: cartQuery(setup),
				movie: movieQuery(setup),
			},
			Mutation: {
				addToCart: addToCartMutation(setup),
				changeCartCount: changeCartCountMutation(setup),
				removeFromCart: removeFromCartMutation(setup),
				async scaffold() {
					await scaffoldDatabase(setup);
					return true;
				},
			},
			CartItem: {},
			Genre: {},
			Movie: {},
		};

		const schema = makeExecutableSchema({
			typeDefs,
			resolvers: (resolvers as any) as IResolvers,
			logger: { log: console.log },
		});

		const server = new ApolloServer({ schema });
		server.applyMiddleware({ app });

		app.listen(APP_API_PORT, () => {
			console.log(
				`Visit ${APP_API_PROTOCOL}://${APP_API_URL}:${APP_API_PORT}/graphql`,
			);
		});
	} catch (e) {
		console.error(e);
	}
};

import axios from "axios";
import { TMDB_URL, TMDB_API_KEY, TMDB_IMAGE_URL } from "./utils/consts";
import { randomNumber } from "./utils/helpers";
import { DBSetup, IMovieEntry } from "./db";
import { ObjectID } from "bson";

export async function populateDatabase(
	{ collections: { Movies } }: DBSetup,
	pageCount: number = 5,
) {
	await Promise.all(
		[...new Array(pageCount)].map((_, j) => j + 1).map(async i => {
			console.info(`Fetching page #${i}`);
			const movies = await axios.get<{ results: any[] }>(
				`${TMDB_URL}/3/discover/movie?sort_by=popularity.desc&page=${i}&api_key=${TMDB_API_KEY}`,
			);
			console.info(
				`Fetched page #${i}, containing ${movies.data.results.length} movies`,
			);

			await Movies.insertMany(
				movies.data.results.map<IMovieEntry>(m => ({
					_id: new ObjectID(),
					tmdb_id: m.id,
					genres: m.genres,
					overview: m.overview,
					posterUrl: `${TMDB_IMAGE_URL}${m.poster_path}`,
					releaseYear: m.release_date && Number(m.release_date.split("-")[0]),
					revenue: m.revenue,
					tagline: m.tagline,
					title: m.title,
					voteAverage: m.voteAverage,
					voteCount: m.voteCount,

					price: randomNumber({ from: 500, to: 1500, step: 50 }),
					stock: randomNumber({ from: 2, to: 5 }),
				})),
			);
			console.info(`Inserted page #${i}`);
		}),
	);
}

export async function clearDatabase({
	collections: { Movies, Carts },
}: DBSetup) {
	await Promise.all([Movies.remove({}), Carts.remove({})]);
	console.info("Cleared database");
}

export async function scaffoldDatabase(setup: DBSetup) {
	await clearDatabase(setup);
	await populateDatabase(setup);
}

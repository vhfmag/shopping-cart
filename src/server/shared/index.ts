import { ObjectID } from "bson";
import { DBSetup } from "../db";
import { CART_TIMEOUT } from "../utils/consts";
import { ExpiredCartError, NotFoundError } from "../errorTypes";
import { throwIfNullish } from "../utils/helpers";

export async function getCartWithId(
	cartId: string | ObjectID,
	{ collections: { Carts } }: DBSetup,
) {
	const lastValidTimestamp = Date.now() - CART_TIMEOUT;

	const cartEntry = await Carts.findOne(new ObjectID(cartId));

	if (cartEntry && cartEntry.lastUpdated.valueOf() < lastValidTimestamp) {
		throw new ExpiredCartError();
	}

	return cartEntry;
}

export async function getRemainingCountOfItem(
	movieId: ObjectID,
	{ collections: { Movies } }: DBSetup,
): Promise<number> {
	const movie = throwIfNullish(
		await Movies.findOne({ _id: movieId }),
		new NotFoundError({ entityName: "movie", id: movieId }),
	);

	return movie.stock;
}

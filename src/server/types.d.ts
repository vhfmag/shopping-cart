export type Omit<Obj, Prop extends keyof Obj> = Pick<
	Obj,
	Exclude<keyof Obj, Prop>
>;

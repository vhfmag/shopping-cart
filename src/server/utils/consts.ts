require("dotenv").config();

export const DATABASE_NAME = "movie_shop";

export const TMDB_IMAGE_URL = "https://image.tmdb.org/t/p/w600_and_h900_bestv2";
export const TMDB_URL = "https://api.themoviedb.org";
export const CART_TIMEOUT = 60e3 * 10;

export const {
	TMDB_API_KEY,
	APP_API_URL = "localhost",
	APP_API_PORT = 3002,
	APP_API_PROTOCOL = "http",
	MONGO_URL = "mongodb://localhost:27017/shopping-cart",
} = process.env;

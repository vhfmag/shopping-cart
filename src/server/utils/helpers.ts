import { ApiError } from "../errorTypes";

export function randomNumber({
	from,
	to,
	step = 1,
}: {
	from: number;
	to: number;
	step?: number;
}) {
	return from + Math.trunc((Math.random() * (to - from)) / step) * step;
}

export function throwIfNullish<T>(
	value: T | null | undefined,
	error: ApiError<any>,
): T {
	if (value === null || value === undefined) {
		throw error;
	}

	return value;
}
